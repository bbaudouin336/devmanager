package fr.bba.devmanager.repository;

import fr.bba.devmanager.entity.Sequence;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SequenceRepository extends MongoRepository<Sequence, String> {

}
