package fr.bba.devmanager.repository;

import fr.bba.devmanager.entity.Project;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProjectRepository extends MongoRepository<Project, String> {

}
