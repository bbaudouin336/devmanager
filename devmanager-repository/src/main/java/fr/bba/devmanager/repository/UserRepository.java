package fr.bba.devmanager.repository;

import fr.bba.devmanager.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

}
