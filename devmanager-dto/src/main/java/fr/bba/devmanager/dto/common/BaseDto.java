package fr.bba.devmanager.dto.common;

public abstract class BaseDto {

    /**
     * Check validity of model
     *
     */
    public abstract boolean isValid();

    /**
     * hashs the code
     */
    public abstract int hashCode();

    /**
     * checks if other object is equal
     *
     * @return true if equals, false otherwise
     */
    public abstract boolean equals(Object obj);
}
