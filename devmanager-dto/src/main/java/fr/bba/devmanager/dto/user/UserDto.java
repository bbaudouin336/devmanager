package fr.bba.devmanager.dto.user;

import fr.bba.devmanager.dto.common.BaseDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserDto extends BaseDto {

    private String id;

    private String lastname;

    private String firstname;

    private String registrationNumber;

    private ProfileDto profile;

    private List<SkillsDto> skills = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public ProfileDto getProfile() {
        return profile;
    }

    public void setProfile(ProfileDto profile) {
        this.profile = profile;
    }

    public List<SkillsDto> getSkills() {
        return skills;
    }

    public void setSkills(List<SkillsDto> skills) {
        this.skills = skills;
    }

    @Override
    public boolean isValid() {

        if (StringUtils.isEmpty(this.lastname)
                || StringUtils.isEmpty(this.firstname)
                || StringUtils.isEmpty(this.registrationNumber)) {
            return false;
        }

        if (this.profile == null || !this.profile.isValid()) {
            return false;
        }

        if (this.skills == null) {
            return false;
        }
        else {
            return this.skills.stream().allMatch(SkillsDto::isValid);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id) && Objects.equals(lastname, userDto.lastname) && Objects.equals(firstname, userDto.firstname);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, lastname, firstname);
    }
}
