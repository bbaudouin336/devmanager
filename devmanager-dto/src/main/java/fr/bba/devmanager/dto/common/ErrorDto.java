package fr.bba.devmanager.dto.common;

public class ErrorDto {

    private String key;

    private String optional;

    public ErrorDto(String key) {
        this.key = key;
    }

    public ErrorDto(String key, String optional) {
        this.key = key;
        this.optional = optional;
    }

    /**
     * Get the key used in the front to get the related message
     *
     * @return The key string
     */
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getOptional() {
        return optional;
    }

    public void setOptional(String optional) {
        this.optional = optional;
    }
}
