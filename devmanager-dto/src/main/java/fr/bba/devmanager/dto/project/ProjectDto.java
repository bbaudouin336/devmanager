package fr.bba.devmanager.dto.project;

import fr.bba.devmanager.dto.common.BaseDto;

import java.util.Objects;

public class ProjectDto extends BaseDto {

    private String id;

    private String registrationNumber;

    private String label;

    private String type;

    private String datemep;

    private String directory;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDatemep() {
        return datemep;
    }

    public void setDatemep(String datemep) {
        this.datemep = datemep;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ProjectDto that = (ProjectDto) o;
        return Objects.equals(id, that.id) && Objects.equals(registrationNumber, that.registrationNumber) && Objects.equals(label, that.label) && Objects.equals(type,
                that.type) && Objects.equals(datemep, that.datemep) && Objects.equals(
                directory,
                that.directory);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, registrationNumber, label, type, datemep, directory);
    }
}
