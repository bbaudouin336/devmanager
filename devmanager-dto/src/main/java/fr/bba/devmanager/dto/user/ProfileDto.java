package fr.bba.devmanager.dto.user;

import fr.bba.devmanager.dto.common.BaseDto;
import org.springframework.util.StringUtils;

import java.util.Objects;

public class ProfileDto extends BaseDto {

    private Long id;

    private String label;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean isValid() {
        return (!StringUtils.isEmpty(id) && !StringUtils.isEmpty(label));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ProfileDto that = (ProfileDto) o;
        return Objects.equals(id, that.id) && Objects.equals(label, that.label);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, label);
    }
}
