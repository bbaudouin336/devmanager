package fr.bba.devmanager.common.tools;


import fr.bba.devmanager.common.exception.impl.DevManagerExceptionImpl;
import fr.bba.devmanager.common.exception.impl.TechnicalKey;

import java.io.File;

/**
 * Class used to manage directories used by XD
 */
public class FileTools {

    /**
     * add full right access to the file
     * @param file file to add rights
     */
    public static void addFullRightFile(File file) throws DevManagerExceptionImpl {

        if (file.exists()) {
            if (!file.setReadable(true, false)) {
                throw new DevManagerExceptionImpl(TechnicalKey.STREAM_FILE_PERMISSION_ERROR);
            }
            if (!file.setWritable(true, false)) {
                throw new DevManagerExceptionImpl(TechnicalKey.STREAM_FILE_PERMISSION_ERROR);
            }
            if (!file.setExecutable(true, false)) {
                throw new DevManagerExceptionImpl(TechnicalKey.STREAM_FILE_PERMISSION_ERROR);
            }
        }

    }
}
