package fr.bba.devmanager.common.tools;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.xobject.PdfImageXObject;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

/**
 * Created by TCS on 08/09/17.
 * <p>
 * Class used to manage pdf creation in xd
 */
public class PdfTools {

    public static final int A4_523_USER_UNITS = 523;

    public static final Float MAIN_SIZE_TEXT = 12f;
    //public static final Float SMALL_SIZE_TEXT = 8f;
    private static final Float TITLE_SIZE_TEXT = 16f;
    //public static final Float SUBTITLE_SIZE_TEXT = 14f;
    //public static final Float DEFAULT_CELL_WIDTH = 100f;

    private static PdfFont mainFont;

    /**
     * init fonts
     *
     * @throws IOException if font creation failed
     */
    public static void init() throws IOException {
        mainFont = PdfFontFactory.createFont(FontProgramFactory.createRegisteredFont(FontConstants.HELVETICA));
    }

    /**
     * generate a title {@link Text} from a text
     *
     * @param text the title
     * @return the title
     */
    public static Text toTitleText(String text) {
        return new Text(text).setFont(mainFont).setBold().setFontSize(TITLE_SIZE_TEXT);
    }

    /**
     * generate a simple {@link Text} from a text
     *
     * @param text     the text
     * @param textSize size of the text
     * @return the simple text
     */
    public static Text toSimpleText(String text, Float textSize) {
        return new Text(text).setFont(mainFont).setFontSize(textSize);
    }

    /**
     * generate a bolded {@link Text} from a text
     *
     * @param text the text
     * @return the bolded text
     */
    public static Text toBoldText(Text text) {
        return text.setBold();
    }

    /**
     * generate an underligned {@link Text} from a text
     *
     * @param text the text
     * @return the underligned text
     */
    public static Text toUnderlignText(Text text) {
        return text.setUnderline();
    }

    /**
     * Create an empty {@link Cell} with his size in columns
     *
     * @param columns number of columns the cell has as size
     * @return the cell
     */
    public static Cell toEmptyCell(int columns) {

        Cell cell = createCellColSpan(columns, new Text(""));
        cell.setHeight(MAIN_SIZE_TEXT);

        return cell;
    }

    /**
     * Create a {@link Cell} with his size in columns and a {@link Paragraph} as content
     *
     * @param colSpan   number of columns the cell has as size
     * @param paragraph the content
     * @return the cell
     */
    public static Cell createCellColSpan(int colSpan, Paragraph paragraph) {
        Cell cell = new Cell(1, colSpan);
        cell.setKeepTogether(true);
        cell.add(paragraph);
        return cell;
    }

    /**
     * Create a {@link Cell} with his size in columns and a {@link Text} as content
     *
     * @param colSpan number of columns the cell has as size
     * @param text    the content
     * @return the cell
     */
    public static Cell createCellColSpan(int colSpan, Text text) {
        Cell cell = new Cell(1, colSpan);
        cell.setKeepTogether(true);
        cell.add(new Paragraph(text).setTextAlignment(TextAlignment.LEFT));
        return cell;
    }

    /**
     * Create a {@link Cell} with his size in columns and a {@link Text} as content and set the {@link TextAlignment}
     *
     * @param colSpan   number of columns the cell has as size
     * @param text      the content
     * @param alignment alignment of test in cell
     * @return the cell
     */
    public static Cell createCellColSpan(int colSpan, Text text, TextAlignment alignment) {
        Cell cell = new Cell(1, colSpan);
        cell.setKeepTogether(true);
        cell.add(new Paragraph(text).setTextAlignment(alignment));
        return cell;
    }

    /**
     * Create a {@link Cell} with his size in columns and a {@link Path} image as content
     *
     * @param colSpan number of columns the cell has as size
     * @param image   the content
     * @return the cell
     * @throws MalformedURLException
     */
    public static Cell createCellWithImageColSpan(int colSpan, Path image) throws MalformedURLException {
        Cell cell = new Cell(1, colSpan);
        cell.setKeepTogether(true);

        PdfImageXObject pdfImageXObject = new PdfImageXObject(ImageDataFactory.create(image.toString()));

        // Link hard coded only for test purpose
        /*PdfImageXObject pdfImageXObject = new PdfImageXObject(ImageDataFactory.create(
                "/home/grdf/Travail/GIT/xd/xd-parent/xd-business/src/main/resources/logo_grdf.png"));*/

        Image pdfImage = new Image(pdfImageXObject, 125);
        pdfImage.setHorizontalAlignment(HorizontalAlignment.CENTER);

        cell.add(pdfImage);
        cell.setPadding(10);
        return cell;
    }

    /**
     * Add a cell to table
     * @param table     Table on which the cell will be add.
     * @param paragraph Paragraph which will be added in the cell
     * @param colspan   number of columns the cell has as size.
     */

    public static void addCellToTable(Table table, Paragraph paragraph, int colspan) {
        Cell colSpanCell = PdfTools.createCellColSpan(colspan, paragraph);
        table.addCell(colSpanCell);
    }

    /**
     * Add a cell to table
     * @param table     Table on which the cell will be add.
     * @param value     Text which will be added in the cell
     * @param colspan   number of columns the cell has as size.
     */

    public static void addCellToTable(Table table, Text value, int colspan) {
        Cell colSpanCell = PdfTools.createCellColSpan(colspan, value);
        table.addCell(colSpanCell);
    }

    /**
     * Add a cell to table
     * @param table     Table on which the cell will be add.
     * @param value     Text which will be added in the cell
     * @param colspan   number of columns the cell has as size
     * @param alignment alignment of test in cell
     */

    public static void addCellToTable(Table table, Text value, int colspan, TextAlignment alignment) {
        Cell colSpanCell = PdfTools.createCellColSpan(colspan, value, alignment);
        table.addCell(colSpanCell);
    }

    /**
     * Add a Cell Colored To Table
     * @param table     Table on which the cell will be add.
     * @param value     Text which will be added in the cell
     * @param colspan   number of columns the cell has as size
     * @param color     Color of the cell.
     * @param alignment alignment of test in cell
     */

    public static void addCellColoredToTable(Table table, Text value, int colspan, Color color, TextAlignment alignment) {
        Cell colSpanCell = PdfTools.createCellColSpan(colspan, value, alignment);
        colSpanCell.setBackgroundColor(color);
        table.addCell(colSpanCell);
    }

    /**
     * Add a Cell With Image
     * @param table     Table on which the cell will be add.
     * @param image     The path of image
     * @param colspan   number of columns the cell has as size
     */

    public static void addCellWithImage(Table table, Path image, int colspan) throws MalformedURLException {
        Cell colSpanCell = PdfTools.createCellWithImageColSpan(colspan, image);
        table.addCell(colSpanCell);
    }

    /**
     * Add a Cell Colored To Table
     * @param table     Table on which the cell will be add.
     * @param value     Text which will be added in the cell
     * @param colspan   number of columns the cell has as size
     * @param color     Color of the cell.
     */
    public static void addCellColoredToTable(Table table, Text value, int colspan, Color color) {
        Cell colSpanCell = PdfTools.createCellColSpan(colspan, value);
        colSpanCell.setBackgroundColor(color);
        table.addCell(colSpanCell);
    }
}
