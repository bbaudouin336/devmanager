package fr.bba.devmanager.common.util;

/**
 * Classe use to manage tuple
 */
public class Tuple<T, U> {


    /**
     * First data of the tuple
     */
    public final T one;

    /**
     * second data of the tuple
     */
    public final U two;

    /**
     * Private constructor
     * @param t tuple's data
     * @param u tuple's data
     */
    private Tuple(T t, U u) {
        this.one = t;
        this.two = u;
    }

    /**
     * Create a new tuple
     * @param t value of the first data
     * @param u value of the second data
     * @return a tuple (t,u)
     */
    public static<T, U> Tuple<T, U> of(T t, U u) {
        return new Tuple<>(t, u);
    }
}
