package fr.bba.devmanager.common.exception.impl;

import fr.bba.devmanager.common.exception.DevManagerException;
import fr.bba.devmanager.common.exception.KeyEnumeration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Main exception implementation to manage error in this application
 */
public class DevManagerExceptionImpl extends Exception implements DevManagerException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3132121905761663872L;

    /**
     * Key of message Bundle
     */
    private final KeyEnumeration message;

    /**
     * Arguments for message
     */
    private Object[] messageArgs;

    private final static Logger LOGGER = LoggerFactory.getLogger(DevManagerExceptionImpl.class);

    /**
     * Constructor
     *
     * @param message     : Key of message bundle
     * @param messageArgs : Args for message
     */
    public DevManagerExceptionImpl(KeyEnumeration message, Object... messageArgs) {
        super(message.getKey());
        this.message = message;
        this.messageArgs = messageArgs;
        LOGGER.error(message.getKey(), message);
    }

    /**
     * Constructor
     *
     * @param message     : Key of message bundle
     * @param throwable   : exception catch
     * @param messageArgs : Args for message
     */
    public DevManagerExceptionImpl(KeyEnumeration message, Throwable throwable, Object... messageArgs) {
        super(message.getKey(), throwable);
        this.message = message;
        this.messageArgs = messageArgs;
        LOGGER.error(throwable.getMessage(),throwable);
    }

    @Override
    public KeyEnumeration getMessageKey() {
        return message;
    }

    @Override
    public Object[] getMessageArgs() {
        return messageArgs;
    }

}
