package fr.bba.devmanager.common.exception;

/**
 * Main interface of xd's exception
 */
public interface DevManagerException {

    /**
     * Return key of message exception
     * 
     * @return
     */
    KeyEnumeration getMessageKey();

    /**
     * Return argument's list of message exception
     * 
     * @return
     */
    Object[] getMessageArgs();

}
