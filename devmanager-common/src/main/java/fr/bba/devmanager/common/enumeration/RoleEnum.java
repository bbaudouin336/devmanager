package fr.bba.devmanager.common.enumeration;

import java.util.Arrays;

/**
 * Enum for Role Entity
 * _C for Create, _U for Update, _R for Read, _D for Delete
 */
public enum RoleEnum {
    UNKNOWN(-1L, "ROLE INCONNU");

    private final String description;
    private final Long id;

    /**
     @ the constructor of ProtysTagsEnum.
     */

    RoleEnum(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    /**
     @ this function return the  RoleEnum based on the id
     */

    public static RoleEnum findById(Long id) {
        return Arrays.stream(values()).filter(r -> r.id.equals(id)).findFirst().orElse(UNKNOWN);
    }

    /**
     *********** GETTERS*********
     */

    public String getDescription() {
        return description;
    }

    public Long getId() {
        return id;
    }
}
