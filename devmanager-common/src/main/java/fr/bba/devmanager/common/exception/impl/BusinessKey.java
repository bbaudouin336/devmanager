package fr.bba.devmanager.common.exception.impl;

import fr.bba.devmanager.common.exception.KeyEnumeration;

/**
 * Business key message error
 */
public enum BusinessKey implements KeyEnumeration {

    // GLOBAL
    MISSING_REQUIRED_FIELD("error.empty.field"),
    ERROR_FORMAT_FIELD("error.format.field"),
    MISSING_ID("error.id"),
    MAX_UPLOAD_SIZE("error.max.upload.size"),

    // User
    USER_MISSING_FIRST_NAME("error.input.user.firstname"),
    USER_MISSING_LAST_NAME("error.input.user.lastname"),
    USER_MISSING_MAIL("error.user.input.mail"),
    USER_MISSING_PROFILE("error.user.input.profile"),
    USER_FORMAT_MAIL("error.user.input.mail.format"),
    USER_MAIL_EXISTS("error.user.input.mail.exists"),
    USER_LOGIN_EXISTS("error.user.input.login.exists"),
    USER_MISSING_FIELD("error.user.missing.field"),
    USER_WAS_DELETED("error.user.was.deleted");

    /**
     * Key of message exception
     */
    private final String key;

    /**
     * Constructor
     *
     * @param key : key of message bundle
     */
    BusinessKey(String key) {
        this.key = key;
    }

    public static BusinessKey getByKey(String key) {
        if (!key.isEmpty()) {
            for (BusinessKey keyException : values()) {
                if (keyException.key.equalsIgnoreCase(key)) {
                    return keyException;
                }
            }
        }
        return MISSING_REQUIRED_FIELD;
    }

    @Override
    public String getKey() {
        return key;
    }
}
