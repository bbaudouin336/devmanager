package fr.bba.devmanager.common.exception;

/**
 * Used to force the template of messages in exceptions
 */
public interface KeyEnumeration {

    String getKey();

}
