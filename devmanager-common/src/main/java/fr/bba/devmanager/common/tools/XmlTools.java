package fr.bba.devmanager.common.tools;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Created by TCS on 05/09/17.
 * <p>
 * Class used to manage xml in XD
 */
public class XmlTools {

    /**
     * init an empty xml document
     *
     * @return the xml document
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public static Document createNewDocument() throws ParserConfigurationException, IOException, SAXException {
        return createNewDocument(null);
    }

    /**
     * init a xml document from a file
     *
     * @return the xml document
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public static Document createNewDocument(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();

        if (file != null) {
            return builder.parse(file);
        } else {
            return builder.newDocument();
        }

    }

    /**
     * Insert a new element with tagName "param" with attribute "name" label and value as content
     *
     * @param document               the xml document
     * @param notificationRequestElt the element
     * @param label                  element's attribute name
     * @param value                  element's content
     */
    public static void insertParamElement(Document document, Element notificationRequestElt, String label, String value) {
        Element notificationQueryElt = document.createElement("param");
        notificationQueryElt.setAttribute("name", label);
        notificationQueryElt.setTextContent(value);
        notificationRequestElt.appendChild(notificationQueryElt);
    }

    /**
     * Insert a new element with tagName "query" with code as content
     *
     * @param document               the xml document
     * @param notificationRequestElt the element
     * @param code                   element's content
     */
    public static void insertQueryElement(Document document, Element notificationRequestElt, String code) {
        Element notificationQueryElt = document.createElement("query");
        notificationQueryElt.setTextContent(code);
        notificationRequestElt.appendChild(notificationQueryElt);
    }

    /**
     * Insert a new element with tagName tagName with value as content
     *
     * @param document the xml document
     * @param elt      the element
     * @param tagName  element's tagName
     * @param value    element's content
     */
    public static void insertXdReplyElement(Document document, Element elt, String tagName, String value) {

        Element replyElt = document.createElement(tagName);
        replyElt.setTextContent(value);
        elt.appendChild(replyElt);

    }

    /**
     * Return the namespace prefix
     *
     * @param doc the xml document
     */
    public static String getOpengisNamespace(Document doc) {

        String prefix = "gml";
        String opengisUrl = "http://www.opengis.net/gml/3.2";
        NamedNodeMap attributes = doc.getFirstChild().getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            Node item = attributes.item(i);
            if (opengisUrl.equals(item.getNodeValue())) {
                String[] parts = item.getNodeName().split(":");
                prefix = parts[1];
                break;
            }
        }

        return prefix;
    }

}
