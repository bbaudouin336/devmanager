package fr.bba.devmanager.common.enumeration;

public enum SequencesEnum {

    SEQ_USERS("user"), SEQ_PROJECT("project"), SEQ_SKILLS("skills");

    private final String seq;

    SequencesEnum(String seq) {
        this.seq = seq;
    }

    public String getSeq() {
        return seq;
    }
}
