package fr.bba.devmanager.common.exception.impl;

import fr.bba.devmanager.common.exception.KeyEnumeration;

/**
 * Key used to explain technical error in this application
 */
public enum TechnicalKey implements KeyEnumeration {

    ID_NOT_FOUND("error.technical.not.indexed"),
    TECHNICAL_ERROR("error.technical"),

    //AUTHENTIFCATION
    BAD_CREDENTIALS("error.bad.credentials"),

    //stream java
    STREAM_FILE_ERROR("error.technical.stream.file"),
    STREAM_FILE_NOT_FOUND("error.technical.stream.file.not.found"),
    STREAM_FILE_PERMISSION_ERROR("error.technical.stream.file.permission"),

    FOLDER_CODE_NOT_EXISTS("error.codes.folder.not.exists"),
    FOLDER_TMP_NOT_EXISTS("error.tmp.folder.not.exists"),
    EXPORT_TEMPLATE_NOT_FOUND("error.export.template.not.found");

    /**
     * Key of message exception
     */
    private final String key;

    /**
     * Constructor
     *
     * @param key : key of message bundle
     */
    TechnicalKey(String key) {
        this.key = key;
    }

    @Override
    public String getKey() {
        return key;
    }

}
