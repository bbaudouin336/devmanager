package fr.bba.devmanager.rest.controller;

import fr.bba.devmanager.business.project.ProjectManager;
import fr.bba.devmanager.common.exception.impl.DevManagerExceptionImpl;
import fr.bba.devmanager.dto.common.ErrorDto;
import fr.bba.devmanager.dto.project.ProjectDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.stream.Collectors;

@RestController("projectServiceRest")
@RequestMapping(path = "/projects")
public class ProjectServiceRest {

    private final static Logger LOGGER = LoggerFactory.getLogger(ProjectServiceRest.class);

    @Autowired
    private ProjectManager projectManager;

    @GetMapping(value = "/getall")
    public ResponseEntity<Object> getAllUsers() {
        try {
            return new ResponseEntity<>(projectManager.getAll()
                    .stream()
                    .sorted(Comparator.comparing(ProjectDto::getId))
                    .collect(Collectors.toList()), HttpStatus.OK);
        }
        catch (DevManagerExceptionImpl x) {
            return new ResponseEntity<>(new ErrorDto(x.getMessage()), HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "/test")
    public ResponseEntity<Object> test() {
        return new ResponseEntity<>("ok", HttpStatus.OK);

    }

}
