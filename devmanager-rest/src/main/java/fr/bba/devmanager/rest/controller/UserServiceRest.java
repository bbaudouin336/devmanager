package fr.bba.devmanager.rest.controller;

import fr.bba.devmanager.business.user.UserManager;
import fr.bba.devmanager.common.exception.impl.BusinessKey;
import fr.bba.devmanager.common.exception.impl.DevManagerExceptionImpl;
import fr.bba.devmanager.dto.common.ErrorDto;
import fr.bba.devmanager.dto.user.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.stream.Collectors;

@RestController("userServiceRest")
@RequestMapping(path = "/users")
public class UserServiceRest {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserServiceRest.class);

    @Autowired
    private UserManager userManager;

    @GetMapping(value = "/getall")
    public ResponseEntity<Object> getAllUsers() {
        try {
            return new ResponseEntity<>(userManager.getAll()
                    .stream()
                    .sorted(Comparator.comparing(UserDto::getId))
                    .collect(Collectors.toList()), HttpStatus.OK);
        }
        catch (DevManagerExceptionImpl x) {
            return new ResponseEntity<>(new ErrorDto(x.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/createOrUpdate")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> createOrUpdateUser(@RequestBody UserDto userDto) {

        if(!userDto.isValid()){
            return new ResponseEntity<>(new ErrorDto(BusinessKey.MISSING_REQUIRED_FIELD.getKey()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        try {
            return new ResponseEntity<>(userManager.createOrUpdateUser(userDto), HttpStatus.CREATED);
        }
        catch (DevManagerExceptionImpl x) {
            return new ResponseEntity<>(new ErrorDto(x.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/test")
    public ResponseEntity<Object> test() {
        return new ResponseEntity<>("ok", HttpStatus.OK);

    }

}
