package fr.bba.devmanager.entity;

import com.querydsl.core.annotations.QueryEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.annotation.Generated;

@QueryEntity
@Document(collection = "project")
public class Project {

    @Id
    @Generated(value = "")
    private String id;

    @Field(value = "registration_number")
    private String registrationNumber;

    @Field(value = "label")
    private String label;

    @Field(value = "type")
    private String type;

    @Field(value = "date_mep")
    private String datemep;

    @Field(value = "directory")
    private String directory;

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDatemep() {
        return datemep;
    }

    public void setDatemep(String datemep) {
        this.datemep = datemep;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }
}
