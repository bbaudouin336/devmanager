package fr.bba.devmanager.entity;

import com.querydsl.core.annotations.QueryEntity;
import fr.bba.devmanager.common.enumeration.SequencesEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@QueryEntity
@Document(collection = "user")
public class User {

    private static final SequencesEnum SEQUENCE_ID = SequencesEnum.SEQ_USERS;

    @Id
    private String id;

    @Field(value = "last_name")
    private String lastname;

    @Field(value = "first_name")
    private String firstname;

    @Field(value = "registration_number")
    private String registrationNumber;

    @DBRef
    private Profile profile;

    @DBRef
    private List<Skills> skills;

    public static SequencesEnum getSequenceId() {
        return SEQUENCE_ID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Skills> getSkills() {
        return skills;
    }

    public void setSkills(List<Skills> skills) {
        this.skills = skills;
    }
}
