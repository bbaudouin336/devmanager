package fr.bba.devmanager;

import fr.bba.devmanager.dto.project.ProjectDto;
import fr.bba.devmanager.dto.user.ProfileDto;
import fr.bba.devmanager.dto.user.SkillsDto;
import fr.bba.devmanager.dto.user.UserDto;
import fr.bba.devmanager.entity.Profile;
import fr.bba.devmanager.entity.Project;
import fr.bba.devmanager.entity.Skills;
import fr.bba.devmanager.entity.User;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = { BusinessConfiguration.class })
public class BusinessConfiguration {

    @Bean
    public static DozerBeanMapper initDozerMapper() {

        DozerBeanMapper mapper = new DozerBeanMapper();

        BeanMappingBuilder builder = new BeanMappingBuilder() {

            protected void configure() {
                mapping(User.class, UserDto.class, TypeMappingOptions.oneWay()).exclude("profile").exclude("skills");
                mapping(UserDto.class, User.class, TypeMappingOptions.oneWay());

                mapping(Profile.class, ProfileDto.class, TypeMappingOptions.oneWay());
                mapping(ProfileDto.class, Profile.class, TypeMappingOptions.oneWay());

                mapping(Skills.class, SkillsDto.class, TypeMappingOptions.oneWay());
                mapping(SkillsDto.class, Skills.class, TypeMappingOptions.oneWay());

                mapping(Project.class, ProjectDto.class, TypeMappingOptions.oneWay());
                mapping(ProjectDto.class, Project.class, TypeMappingOptions.oneWay());
            }
        };

        mapper.addMapping(builder);
        return mapper;
    }

}
