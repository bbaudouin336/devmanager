package fr.bba.devmanager.business.generic.impl;

import fr.bba.devmanager.business.generic.inter.GenericManager;
import fr.bba.devmanager.common.enumeration.SequencesEnum;
import fr.bba.devmanager.entity.Sequence;
import fr.bba.devmanager.repository.SequenceRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class GenericManagerImpl implements GenericManager {

    @Autowired
    protected DozerBeanMapper dozerMapper;

    @Autowired
    private SequenceRepository sequenceRepository;

    protected String generateSequence(SequencesEnum sequencesEnum) {

        if (sequenceRepository.findById(sequencesEnum.getSeq()).isPresent()) {
            Sequence sequence = sequenceRepository.findById(sequencesEnum.getSeq()).get();
            long newSeq = sequence.getSeq() + 1;
            sequence.setSeq(newSeq);
            sequenceRepository.save(sequence);
            return "" + newSeq;
        }
        else {
            Sequence sequence = new Sequence();
            sequence.setSeq(1L);
            sequence.setId(sequencesEnum.getSeq());
            sequenceRepository.insert(sequence);
            return "1";
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DozerBeanMapper getDozerMapper() {
        return dozerMapper;
    }

    /**
     * Public setter, used in test.
     * @param dozerMapper the dozer mapper
     */
    public void setDozerMapper(DozerBeanMapper dozerMapper) {
        this.dozerMapper = dozerMapper;
    }
}
