package fr.bba.devmanager.business.user;

import fr.bba.devmanager.business.generic.impl.GenericManagerImpl;
import fr.bba.devmanager.common.exception.impl.BusinessKey;
import fr.bba.devmanager.common.exception.impl.DevManagerExceptionImpl;
import fr.bba.devmanager.common.exception.impl.TechnicalKey;
import fr.bba.devmanager.dto.user.ProfileDto;
import fr.bba.devmanager.dto.user.SkillsDto;
import fr.bba.devmanager.dto.user.UserDto;
import fr.bba.devmanager.entity.Profile;
import fr.bba.devmanager.entity.Skills;
import fr.bba.devmanager.entity.User;
import fr.bba.devmanager.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service("userManager")
public class UserManager extends GenericManagerImpl {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserManager.class);

    @Autowired
    private UserRepository userRepository;


    /**
     * Returns all Users from database
     * @return
     * @throws DevManagerExceptionImpl
     */
    public List<UserDto> getAll() throws DevManagerExceptionImpl {

        try {
            List<User> lstUsr = userRepository.findAll();
            return this.getFullDtoList(lstUsr);
        }
        catch (Exception e) {
            throw new DevManagerExceptionImpl(TechnicalKey.TECHNICAL_ERROR, e, e.getMessage());
        }

    }

    /**
     * Insert or Update a user in database
     * @param userDto
     * @return
     */
    public UserDto createOrUpdateUser(UserDto userDto) throws DevManagerExceptionImpl {

        if(!userDto.isValid()){
            throw new DevManagerExceptionImpl(BusinessKey.MISSING_REQUIRED_FIELD);
        }

        try {
            User toSave = getFullEntity(userDto);

            if(userDto.getId() == null || !userRepository.findById(userDto.getId()).isPresent()){
                toSave.setId(this.generateSequence(User.getSequenceId()));
            }

            return getFullDto(userRepository.save(toSave));
        }
        catch (Exception e) {
            throw new DevManagerExceptionImpl(TechnicalKey.TECHNICAL_ERROR, e, e.getMessage());
        }

    }

    /**
     * returns UserDto List. Children are fetched for each element
     *
     * @param lstEntity
     * @return
     */
    private List<UserDto> getFullDtoList(List<User> lstEntity) {

        List<UserDto> toReturn = new ArrayList<>();
        for (User entity : lstEntity) {
            toReturn.add(getFullDto(entity));
        }
        return toReturn;
    }

    /**
     * Converts given entity to dto (and returns it), with children instances
     *
     * @param userEntity entity to convert
     * @return corresponding UserDto
     */
    private UserDto getFullDto(User userEntity) {

        UserDto toReturn = dozerMapper.map(userEntity, UserDto.class);
        if (userEntity.getProfile() != null) {
            toReturn.setProfile(dozerMapper.map(userEntity.getProfile(), ProfileDto.class));
        }

        if (userEntity.getSkills() != null) {
            userEntity.getSkills().forEach(skills -> toReturn.getSkills().add(dozerMapper.map(skills, SkillsDto.class)));
        }

        return toReturn;
    }

    /**
     * Converts given dto to entity (and returns it), with children instances
     * @param userDto dto to convert
     * @return corresponding User
     */
    private User getFullEntity(UserDto userDto) {

        User toReturn = dozerMapper.map(userDto, User.class);
        if (userDto.getProfile() != null) {
            toReturn.setProfile(dozerMapper.map(userDto.getProfile(), Profile.class));
        }

        if (userDto.getSkills() != null) {
            userDto.getSkills().forEach(skills -> toReturn.getSkills().add(dozerMapper.map(skills, Skills.class)));
        }

        return toReturn;

    }
}
