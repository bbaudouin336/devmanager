package fr.bba.devmanager.business.project;

import fr.bba.devmanager.business.generic.impl.GenericManagerImpl;
import fr.bba.devmanager.common.exception.impl.DevManagerExceptionImpl;
import fr.bba.devmanager.common.exception.impl.TechnicalKey;
import fr.bba.devmanager.dto.project.ProjectDto;
import fr.bba.devmanager.entity.Project;
import fr.bba.devmanager.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service("projectManager")
public class ProjectManager extends GenericManagerImpl {

    private final static Logger LOGGER = LoggerFactory.getLogger(ProjectManager.class);

    @Autowired
    private ProjectRepository projectRepository;

    public List<ProjectDto> getAll() throws DevManagerExceptionImpl {

        try {
            List<Project> lstPrj = projectRepository.findAll();
            return this.getFullDtoList(lstPrj);
        }
        catch (Exception e) {
            throw new DevManagerExceptionImpl(TechnicalKey.TECHNICAL_ERROR, e, e.getMessage());
        }
    }

    /**
     * returns ProjectDto List. Children are fetched for each element
     *
     * @param lstProject
     * @return
     */
    private List<ProjectDto> getFullDtoList(List<Project> lstProject) {
        List<ProjectDto> toReturn = new ArrayList<>();
        for (Project entity : lstProject) {
            toReturn.add(getFullDto(entity));
        }
        return toReturn;
    }

    /**
     * Converts given entity to dto (and returns it), with children instances
     *
     * @param projectEntity entity to convert
     * @return corresponding ProjectDto
     */
    private ProjectDto getFullDto(Project projectEntity) {
        return dozerMapper.map(projectEntity, ProjectDto.class);
    }

}
