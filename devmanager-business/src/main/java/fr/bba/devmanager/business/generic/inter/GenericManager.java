package fr.bba.devmanager.business.generic.inter;

import org.dozer.DozerBeanMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface GenericManager {

    /**
     * Loops in a list and dozer maps each element to destination type
     * @param source source {@link List} of object
     * @param destType type of the objects mapped in output list
     * @return {@link ArrayList} of mapped object
     */
    default <T, U> List<U> dozerMapList(final List<T> source, final Class<U> destType) {
        return Optional.ofNullable(source)
                .orElse(new ArrayList<>())
                .stream()
                .map(src -> getDozerMapper().map(src, destType))
                .collect(Collectors.toList());
    }

    /**
     * Return the dozer mapper
     * @return
     */
    DozerBeanMapper getDozerMapper();

}
