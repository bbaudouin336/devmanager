import {FETCH_MENUTABS_SELECTED, FETCH_RESET_SCREEN, FETCH_MENUTABS_DEFAULT, FETCH_MENU_TAB, TABS_KEY} from '../constantes/MenuConstant';
import {LABELS} from "../constantes/Globals";

const INITIAL_STORE = {
    selectedTab: {key: TABS_KEY.MANAGE_ASSIGNMENT, label: LABELS.manageAssignment, index: 0},
    loaded: false
};

export default function ApplicationReducer(state = INITIAL_STORE, action) {
    switch (action.type) {
        case FETCH_MENU_TAB:
            return { ...state, menuTabs: action.payload.menuTabs };
        case FETCH_MENUTABS_SELECTED:
            return { ...state, selectedTab: action.payload.selectedTab };
        case FETCH_MENUTABS_DEFAULT:
            return { ...state, defaultItem: action.payload.defaultItem };
        case FETCH_RESET_SCREEN:
            return { ...state, resetScr: action.payload.resetScr };
        default: return state;
    }
};
