import {FETCH_CREATEORUPDATE, FETCH_SELECTED_USER, FETCH_USERS,FETCH_UDPATEFORMUSER} from '../constantes/UserConstant';


const INITIALSTATE = {
    users: undefined,
    selectedUser: 0,
    user: undefined
};


export default function UserReducer(state = INITIALSTATE, action) {
    // console.log(action.payload);
    switch (action.type) {
        case FETCH_CREATEORUPDATE:
            return {...state, user: action.payload.user};
        case FETCH_USERS:
            return {...state, users: action.payload.users};
        case FETCH_SELECTED_USER :
            return {...state, selectedUser: action.payload.selectedUser};
        case FETCH_UDPATEFORMUSER:
            return {...state, userForm: action.payload.userForm};
        default:
            return state;
    }
}
