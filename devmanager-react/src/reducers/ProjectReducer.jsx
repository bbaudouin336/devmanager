import {FETCH_PROJECTS, FETCH_SELECTED_PROJECT} from '../constantes/ProjectConstant';


const INITIALSTATE = {
    projects: undefined,
    selectedUser: 0
};


export default function ProjectReducer(state = INITIALSTATE, action) {
    switch (action.type) {
        case FETCH_PROJECTS:
            return {...state, projects: action.payload.projects};
        case FETCH_SELECTED_PROJECT :
            return {...state, selectedProject: action.payload.selectedProject};
        default:
            return state;
    }
}
