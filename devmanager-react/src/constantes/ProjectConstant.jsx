export const FETCH_PROJECTS = "PROJECTS";
export const FETCH_SELECTED_PROJECT = "SELECTED_PROJECT";
export const REST_CALL_API_PROJECT = process.env.REACT_APP_BACK_URL + "/projects/";
