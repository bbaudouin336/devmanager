//Classe des labels communs à toute la fonctionnalité "Gestion des ressources" / package User

export const UserGlobals = {

    lastname: 'Nom de famille',
    firstname: 'Prénom',
    registrationNumber: 'Matricule',
    profile: 'Fonction',
    skills: 'Compétences',
    addSkill :"Ajouter compétence"

};
