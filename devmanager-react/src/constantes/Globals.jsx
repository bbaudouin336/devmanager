//Classe des labels communs à toute l'application

export const Globals = {

    //-----------ERRORS------------------
    errorGeneric: 'Une erreur s\'est produite. Veuillez redémarrer l\'application et réessayez.',
    errorsEmailEmpty: 'Au moins une adresse e-mail destinataire doit être renseignée',
    errorsEmailWrong: 'L\'adresse e-mail est incorrecte',
    errorFieldEmpty: 'Le champ est obligatoire',
    errorFieldNotAlpha: 'Le champ ne doit contenir que des lettres',

    //-----------SUCCESS------------------    
    sucessLabel: 'exemple de lable de success',

    //-----------MISC------------------
    exempleMail: 'exemple@email.fr',
    loadingApplication: 'Chargement de l\'application en cours...',
};

export const BUTTONS = {
    buttonValidate: 'Valider',
    buttonSend: 'Envoyer',
    buttonYes: 'Oui',
    buttonNo: 'Non',
    buttonCancel : 'Annuler',
    buttonClose : 'Fermer',
    buttonDelete : 'Supprimer',
};

export const LABELS = {
    sujet: "Sujet",
    message: "Message",
    fax: "Numéro de fax du destinataire",
    mail: "Mail du destinataire",
    manageAssignment : "Gérer allocations",
    profile : "Profil",
    managePeople : "Gérer ressources",
    manageProjects : "Gérer projets"
};
