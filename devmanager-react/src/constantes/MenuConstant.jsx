import {LABELS} from './Globals';

export const FETCH_MENU_TAB = "MENU_TAB";
export const FETCH_MENUTABS_SELECTED = "MENUTABS_SELECTED";
export const FETCH_MENUTABS_DEFAULT = "MENUTABS_DEFAULT";
export const FETCH_RESET_SCREEN = "RESET_SCREEN";


export const TABS_KEY = {
    MANAGE_ASSIGNMENT: "manageAssignment",
    PROFILE: "profile",
    MANAGE_PEOPLE: "managePeople",
    MANAGE_PROJECTS: "manageProjects",

};

export const MENU_TABS = [
    {key: TABS_KEY.MANAGE_ASSIGNMENT, label: LABELS.manageAssignment, index: 0},
    {key: TABS_KEY.PROFILE, label: LABELS.profile, index: 1},
    {key: TABS_KEY.MANAGE_PEOPLE, label: LABELS.managePeople, index: 2},
    {key: TABS_KEY.MANAGE_PROJECTS, label: LABELS.manageProjects, index: 3}
];


export function getMenuTabsByIndex(index) {
    let tabToReturn = undefined;
    MENU_TABS.forEach(tab => {
        if (tab.index === index) {
            tabToReturn = tab;
        }
    });
    return tabToReturn;
}

export function getMenuTabsByKey(tabsKey) {
    let tabToReturn = undefined;
    MENU_TABS.forEach(tab => {
        if (tab.key === tabsKey) {
            tabToReturn = tab;
        }
    });
    return tabToReturn;
}
