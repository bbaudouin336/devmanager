import {FETCH_MENUTABS_SELECTED, getMenuTabsByIndex} from '../constantes/MenuConstant';

export const fetchSelectedTab= selectedTab => ({
    type: FETCH_MENUTABS_SELECTED,
    payload: { selectedTab }
});


export function updateSelectedTab(selectedTab) {
    return (dispatch) => {
        if(selectedTab === undefined){
            dispatch(fetchSelectedTab(getMenuTabsByIndex(0)));
        }else{
            dispatch(fetchSelectedTab(getMenuTabsByIndex(selectedTab)));
        }
    }
}
