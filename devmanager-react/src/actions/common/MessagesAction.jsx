
const append = (type, message) => {
    return function(dispatch) {
        dispatch({
            type: type,
            messages: Array.isArray(message) ? message : [ message ]
        });
    }
}

export const warning = (message) => {
    return append('WARNING', message);
};

export const info = (message) => {
    return append('INFO', message);
};

export const message = (message) => {
    return append('MESSAGE', message);
};

export const success = (message) => {
    return append('SUCCESS', message);
};

export const error = (message) => {
    return append('ERROR', message);
}

export const clear = () => {
    return function(dispatch) {
        dispatch({ type: 'CLEAR_ALL' });
    }
}

export const clearMessage = (type) => {
    return function(dispatch) {
        dispatch({ type: 'CLEAR', kind: type });
    }
}