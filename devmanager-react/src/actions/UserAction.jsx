import {FETCH_CREATEORUPDATE, FETCH_SELECTED_USER, FETCH_UDPATEFORMUSER, FETCH_USERS, REST_CALL_API_USER} from '../constantes/UserConstant';

export const fetchUsers = users => ({
    type: FETCH_USERS,
    payload: {users}
});

export const fetchSelectedUser = selectedUser => ({
    type: FETCH_SELECTED_USER,
    payload: {selectedUser}
});

export const fetchCreateOrUpdateUsers = userSaved => ({
    type: FETCH_CREATEORUPDATE,
    payload: {userSaved}
});
export const fetchUpdateFormUser = userForm => ({
    type: FETCH_UDPATEFORMUSER,
    payload: {userForm}
});


//-----------CRUD------------------------------
/**
 * Get all users in database
 * @returns {function(*): Promise<Response>}
 */
export function getallUsers() {

    return (dispatch) => {
        return fetch(REST_CALL_API_USER + 'getall', {

            method: 'GET'
        }).then(response => {
            if (response.status > 400) {
                // Une erreur s'est produite
                // gérer les messages d'erreur
                // response.json().then(reason => {
                //     dispatch(Messages.error(reason.message));
                // });
                return Promise.reject();
            }
            return response.json();
        }).then(users => {
            dispatch(fetchUsers(users));
            return users;
        })

    }

}

/**
 * Create or update a user
 * @param userToSave
 */
export function createOrUpdate(userToSave) {

    return (dispatch) => {
        return fetch(REST_CALL_API_USER + 'createOrUpdate', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userToSave)
        }).then(response => {
            if (response.status > 400) {
                // Une erreur s'est produite
                // gérer les messages d'erreur
                // response.json().then(reason => {
                //     dispatch(Messages.error(reason.message));
                // });
                return Promise.reject();
            }
            return response.json();
        }).then(userSaved => {
            dispatch(fetchCreateOrUpdateUsers(userSaved));
            return userSaved;
        })

    }

}

//-----------MISC------------------------------

/**
 * update the selected user for the front, allow to know which tab to show and user's data
 * @param selectedUser
 * @returns {Function}
 */
export function updateSelectedUser(selectedUser) {
    return (dispatch) => {
        if (selectedUser === undefined) {
            dispatch(fetchSelectedUser(1));
        } else {
            dispatch(fetchSelectedUser(selectedUser));
        }
    }
}

export function updateFormUser(userForm) {
    // console.log(userForm);
    return (dispatch) => {
        dispatch(fetchUpdateFormUser(userForm));
    }
}


