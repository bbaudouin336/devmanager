import {FETCH_PROJECTS,FETCH_SELECTED_PROJECT, REST_CALL_API_PROJECT} from '../constantes/ProjectConstant';



export const fetchProjects = projects => ({
    type: FETCH_PROJECTS,
    payload: {projects}
});

export const fetchSelectedProject = selectedProject => ({
    type: FETCH_SELECTED_PROJECT,
    payload: {selectedProject}
});

export function getallProjects() {

    return (dispatch) => {
        return fetch(REST_CALL_API_PROJECT + 'getall', {

            method: 'GET'
        }).then(response => {
            if (response.status > 400) {
                // Une erreur s'est produite
                // gérer les messages d'erreur
                // response.json().then(reason => {
                //     dispatch(Messages.error(reason.message));
                // });
                return Promise.reject();
            }
            return response.json();
        }).then(projects => {
            dispatch(fetchProjects(projects));
            return projects;
        })
    }
}

export function updateSelectedProject(selectedProject) {
    return (dispatch) => {
        if (selectedProject === undefined) {
            dispatch(fetchSelectedProject(1));
        } else {
            dispatch(fetchSelectedProject(selectedProject));
        }
    }
}
