import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getMenuTabsByKey, MENU_TABS, TABS_KEY} from '../../constantes/MenuConstant';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import {updateSelectedTab} from '../../actions/ApplicationAction';

class MenuPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            defaultTab: getMenuTabsByKey(TABS_KEY.MANAGE_ASSIGNMENT).index
        };
        this.handleChange = this.handleChange.bind(this);
    }


    componentDidMount() {
        this.props.updateSelectedTab();
    }

    handleChange(event, newValue) {
        this.props.updateSelectedTab(newValue);
    }

    render() {
        return (
            <Tabs
                orientation="vertical"
                variant="scrollable"
                value={this.props.selectedTab === undefined ? this.state.defaultTab : this.props.selectedTab.index}
                onChange={this.handleChange}
                className="menu-app"
            >
                {
                    MENU_TABS.map(tab =>
                        <Tab label={tab.label} key={tab.index} id={tab.index} className={this.props.selectedTab.index !== parseInt(tab.index,10) ? "menu-button" : "menu-button active"}/>
                    )
                }

            </Tabs>
        );
    }
}

const mapStateToProps = state => ({
    menuTabs: state.applicationReducer.menuTabs,
    selectedTab: state.applicationReducer.selectedTab
});

const mapDispatchToProps = dispatch => bindActionCreators({
    updateSelectedTab
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MenuPage);
