import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';

class HeaderPage extends React.Component {

    componentDidMount() {
    }

    render() {
        return (
            <AppBar position="static">
                <Toolbar className="header-app">
                    <Typography variant="h6" className="title-text">
                        {this.props.selectedTab.label}
                    </Typography>
                    <IconButton>
                        <AccountCircle />
                    </IconButton>
                </Toolbar>
            </AppBar>
        );
    }

}

const mapStateToProps = state => ({
    selectedTab: state.applicationReducer.selectedTab
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HeaderPage);
