// import React from 'react';
// import * as Redux from 'react-redux';
// import * as Actions from '../../actions/common/MessagesAction';
// import { Message } from 'semantic-ui-react';


// class Messages extends React.Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//             messages: props.messages ? props.messages : undefined
//         }
//     }

//     componentWillUpdate(props) {
//         return this.state.messages !== props.messages;
//     }

//     componentWillReceiveProps(props) {
//         if (this.state.messages !== props.messages) {
//             this.setState({ messages: props.messages });
//         }
//     }

//     handleDismiss(type) {
//         this.props.clearMessage(type);
//     }

//     render() {

//         let content = [];
//         if (this.state.messages) {
//             if (this.state.messages.error.length) {
//                 content.push(<Message key="error" error onDismiss={this.handleDismiss.bind(this, 'ERROR')}>
//                     <Message.Header>Une erreur est survenue</Message.Header>
//                     {this.state.messages.error.length > 1 ? <Message.List items={this.state.messages.error.map(message => message.text)} /> : this.state.messages.error[0].text}
//                 </Message>);
//             }
//             if (this.state.messages.warning.length) {
//                 content.push(<Message key="warning" warning onDismiss={this.handleDismiss.bind(this, 'WARNING')}>
//                     <Message.Header>Votre attention est requise</Message.Header>
//                     {this.state.messages.warning.length > 1 ? <Message.List items={this.state.messages.warning.map(message => message.text)} /> : this.state.messages.warning[0].text}
//                 </Message>);
//             }
//             if (this.state.messages.success.length) {
//                 content.push(<Message key="success" success onDismiss={this.handleDismiss.bind(this, 'SUCCESS')}>
//                     <Message.Header>Terminé avec succès</Message.Header>
//                     {this.state.messages.success.length > 1 ? <Message.List items={this.state.messages.success.map(message => message.text)} /> : this.state.messages.success[0].text}
//                 </Message>);
//             }
//             if (this.state.messages.info.length) {
//                 content.push(<Message key="info" info onDismiss={this.handleDismiss.bind(this, 'INFO')}>
//                     <Message.Header>Information</Message.Header>
//                     {this.state.messages.info.length > 1 ? <Message.List items={this.state.messages.info.map(message => message.text)} /> : this.state.messages.info[0].text}
//                 </Message>);
//             }
//             if (this.state.messages.message.length) {
//                 content.push(<Message key="message" onDismiss={this.handleDismiss.bind(this, 'MESSAGE')}>
//                     <Message.Header>Information</Message.Header>
//                     {this.state.messages.message.length > 1 ? <Message.List items={this.state.messages.message.map(message => message.text)} /> : this.state.messages.message[0].text}
//                 </Message>);
//             }
//         }

//         return <div className="new-zone_messages">{content}</div>;
//     }

// }

// export default Redux.connect(
//     function (state) {
//         return {
//             messages: state.messages
//         };

//     }, function (dispatch) {
//         return {
//             clear: () => { dispatch(Actions.clear()) },
//             clearMessage: (type) => { dispatch(Actions.clearMessage(type)) }
//         }

//     })(Messages);