import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';


import {Button, Card, CardContent, CardHeader, Grid, List, ListItemIcon, ListItem,ListItemText, Paper,IconButton} from '@material-ui/core';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ControlPointOutlinedIcon from '@material-ui/icons/ControlPointOutlined';
import {BUTTONS} from "../../../constantes/Globals";

class AssignmentPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.addUserToProject = this.addUserToProject.bind(this);
    }

    componentDidMount() {
    }

    addUserToProject(event,idProject){
        console.log(idProject);
    }

    render() {
        if (this.props.projects !== undefined && this.props.selectedProject !== 99) {
            return (
                <Paper className="panel-container">
                    <Grid container spacing={1}>
                        {
                            this.props.projects.map((project, index) =>
                                <Grid key={index} item xs={3}>
                                    <Card className="project-container">
                                        <CardHeader title={project.label}/>
                                        <CardContent>
                                            <List disablePadding>
                                                <ListUsers users={this.props.users} />
                                                <ListItem dense disableGutters>
                                                    <ListItemIcon>
                                                        <IconButton onClick={e => this.addUserToProject(e,project.id)}>
                                                            <ControlPointOutlinedIcon  className="btn-add"/>
                                                        </IconButton>
                                                    </ListItemIcon>
                                                </ListItem>
                                            </List>
                                        </CardContent>
                                    </Card>
                                </Grid>
                            )
                        }
                        <Grid item xs={12}>
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs={3}>
                            <Button variant="contained" size="small" className="btn btn-secondary">
                                {BUTTONS.buttonCancel}
                            </Button>
                        </Grid>
                        <Grid item xs={3}>
                            <Button variant="contained" size="small" className="btn btn-primary">
                                {BUTTONS.buttonValidate}
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
            );
        } else {
            return (
                <Card className="panel-container">
                </Card>
            );
        }
    }
}


export function ListUsers(props) {
    if (props.users === undefined) {
        return (
            <ListItem dense disableGutters>
                <ListItemText primary="Ajouter des développeurs"/>
            </ListItem>
        );
    } else {
        return (
            props.users.map(user =>
                <ListItem key={user.id}  dense disableGutters divider>
                    <ListItemIcon>
                        <IconButton >
                            <HighlightOffIcon className="btn-red" />
                        </IconButton>
                    </ListItemIcon>
                    <ListItemText id={user.id} primary={user.registrationNumber}/>
                </ListItem>
            )
        );
    }
}

const mapStateToProps = state => ({
    users: state.userReducer.users,
    projects: state.projectReducer.projects
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentPanel);
