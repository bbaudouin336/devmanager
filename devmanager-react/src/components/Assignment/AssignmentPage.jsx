import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Container from '@material-ui/core/Container';

import {getMenuTabsByKey, TABS_KEY} from '../../constantes/MenuConstant';

import AssignmentPanel from './subComponents/AssignmentPanel';

class AssignmentPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            indexTab: getMenuTabsByKey(TABS_KEY.MANAGE_ASSIGNMENT).index
        };
    }

    render() {
        return (

            <Container
                component="div"
                role="tabpanel"
                hidden={this.props.selectedTab.index !== this.state.indexTab}
                id={`vertical-tabpanel-${this.state.indexTab}`}
            >
                <AssignmentPanel/>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    selectedTab: state.applicationReducer.selectedTab,
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentPage);
