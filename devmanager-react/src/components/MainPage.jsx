import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import UserPage from './User/UserPage';
import ProjectPage from './Project/ProjectPage';
import AssignmentPage from './Assignment/AssignmentPage';
import HeaderPage from './common/HeaderPage';
import MenuPage from './common/MenuPage';

class MainPage extends React.Component {

    componentDidMount() {
    }

    render() {
        return (
            <div>
                <HeaderPage/>
                <MenuPage/>
                <div className="main-container">
                    <UserPage/>
                    <ProjectPage/>
                    <AssignmentPage/>
                </div>
            </div>
        );
    }

}


const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
