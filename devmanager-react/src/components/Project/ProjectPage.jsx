import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Container from '@material-ui/core/Container';

import {getMenuTabsByKey, TABS_KEY} from '../../constantes/MenuConstant';

import ProjectTab from './subComponents/ProjectTab';
import ProjectPanel from './subComponents/ProjectPanel';


class ProjectPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            indexTab: getMenuTabsByKey(TABS_KEY.MANAGE_PROJECTS).index
        };
    }

    render() {
        return (

            <Container
                component="div"
                role="tabpanel"
                hidden={this.props.selectedTab.index !== this.state.indexTab}
                id={`vertical-tabpanel-${this.state.indexTab}`}
            >
                <ProjectTab/>
                <ProjectPanel/>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    selectedTab: state.applicationReducer.selectedTab,
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProjectPage);
