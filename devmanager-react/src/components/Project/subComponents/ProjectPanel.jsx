import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Paper from '@material-ui/core/Paper';

import ProjectForm from './ProjectForm';

class ProjectPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
    }

    render() {
        if (this.props.projects !== undefined && this.props.selectedProject !== 99) {
            return (
                this.props.projects.map(project =>
                    <Paper
                        component="div"
                        hidden={this.props.selectedProject !== parseInt(project.id, 10)}
                        id={`projectpanel-${project.id}`}
                        key={project.id}
                        className="panel-container"
                    >
                        <ProjectForm project={project}/>
                    </Paper>
                )
            );
        } else {
            return (
                <Paper className="panel-container" >
                    <ProjectForm/>
                </Paper>
            );
        }
    }
}

const mapStateToProps = state => ({
    projects: state.projectReducer.projects,
    selectedProject: state.projectReducer.selectedProject
});

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProjectPanel);
