import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Container from '@material-ui/core/Container';
import ControlPointOutlinedIcon from '@material-ui/icons/ControlPointOutlined';

import {getallProjects, updateSelectedProject} from '../../../actions/ProjectAction';


class ProjectTab extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.props.getallProjects();
        this.props.updateSelectedProject();
    }

    handleChange(event, newValue) {
        this.props.updateSelectedProject(newValue);
    }

    render() {
        if (this.props.projects !== undefined) {
            return (
                <div className="tab-app">
                    <Tabs
                        value={this.props.selectedProject === undefined ? 1 : this.props.selectedProject}
                        onChange={this.handleChange}
                    >
                        {
                            this.props.projects.map(project =>
                                <Tab label={project.registrationNumber} key={parseInt(project.id, 10)} value={parseInt(project.id, 10)} className="tab-button"/>
                            )
                        }
                        <Tab icon={<ControlPointOutlinedIcon  className="btn-add"/>} value={99} className="tab-button" />
                    </Tabs>
                </div>
            );
        } else {
            return (
                <Container>Can't reach the project list</Container>
            );
        }

    }

}

const mapStateToProps = state => ({
    projects: state.projectReducer.projects,
    selectedProject: state.projectReducer.selectedProject
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getallProjects,
    updateSelectedProject
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProjectTab);
