import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import {BUTTONS} from "../../../constantes/Globals";


class ProjectForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
    }

    render() {
        if (this.props.project !== undefined) {
            return (<UpdateForm project={this.props.project}/>)
        }else{
            return (<CreateForm project={this.props.project}/>)
        }
    }
}

export function UpdateForm(props) {
    let project = props.project;

    return (
        <form noValidate autoComplete="off">
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <TextField
                        id="standard-lastname"
                        label="Nom"
                        value={project.label}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        id="standard-firstname"
                        label="Type"
                        value={project.type}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        id="standard-registrationNumber"
                        label="Numéro"
                        value={project.registrationNumber}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        id="standard-profile"
                        label="Date MEP"
                        value={project.datemep}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        id="standard-profile"
                        label="Dossier projet"
                        value={project.directory}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={12}>
                </Grid>
                <Grid item xs={6}>
                    <Button variant="contained" color="secondary" className="btn">
                        {BUTTONS.buttonDelete}
                        <DeleteIcon/>
                    </Button>
                </Grid>
                <Grid item xs={3}>
                    <Button variant="contained" size="small" className="btn btn-secondary">
                        {BUTTONS.buttonCancel}
                    </Button>
                </Grid>
                <Grid item xs={3}>
                    <Button variant="contained" size="small" className="btn btn-primary">
                        {BUTTONS.buttonValidate}
                    </Button>
                </Grid>
            </Grid>
        </form>

    );
}


export function CreateForm(props) {
    return (
        <form noValidate autoComplete="off">
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <TextField
                        id="standard-lastname"
                        label="Nom"
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        id="standard-firstname"
                        label="Type"
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        id="standard-registrationNumber"
                        label="Numéro"
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        id="standard-profile"
                        label="Date MEP"
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        id="standard-profile"
                        label="Dossier projet"
                        margin="normal"
                    />
                </Grid>

                <Grid item xs={12}>
                </Grid>
                <Grid item xs={6}>
                </Grid>
                <Grid item xs={3}>
                    <Button variant="contained" size="small" className="btn btn-secondary">
                        {BUTTONS.buttonCancel}
                    </Button>
                </Grid>
                <Grid item xs={3}>
                    <Button variant="contained" size="small" className="btn btn-primary">
                        {BUTTONS.buttonValidate}
                    </Button>
                </Grid>
            </Grid>
        </form>

    );
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProjectForm);
