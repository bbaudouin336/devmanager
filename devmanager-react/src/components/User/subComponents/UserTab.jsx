import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Container from '@material-ui/core/Container';
import ControlPointOutlinedIcon from '@material-ui/icons/ControlPointOutlined';

import {getallUsers, updateSelectedUser} from '../../../actions/UserAction';


class UserTab extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.props.getallUsers();
        this.props.updateSelectedUser();
    }

    handleChange(event, newValue) {
        this.props.updateSelectedUser(newValue);
    }

    render() {
        if (this.props.users !== undefined) {
            return (
                <div className="tab-app">
                    <Tabs
                        value={this.props.selectedUser === undefined ? 1 : this.props.selectedUser}
                        onChange={this.handleChange}
                    >
                        {
                            this.props.users.map(user =>
                                <Tab label={user.registrationNumber} key={parseInt(user.id, 10)} value={parseInt(user.id, 10)} className="tab-button"/>
                            )
                        }
                        <Tab icon={<ControlPointOutlinedIcon  className="btn-add"/>} value={99} className="tab-button" />
                    </Tabs>
                </div>
            );
        } else {
            return (
                <Container>Loading</Container>
            );
        }

    }

}

const mapStateToProps = state => ({
    users: state.userReducer.users,
    selectedUser: state.userReducer.selectedUser
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getallUsers,
    updateSelectedUser
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserTab);
