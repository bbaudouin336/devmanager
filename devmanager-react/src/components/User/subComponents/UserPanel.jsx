import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Paper from '@material-ui/core/Paper';

import UserForm from './UserForm';

class UserPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
    }

    render() {
        if (this.props.users !== undefined && this.props.selectedUser !== 99) {
            return (
                this.props.users.map(user =>
                    <Paper
                        component="div"
                        hidden={this.props.selectedUser !== parseInt(user.id, 10)}
                        id={`userpanel-${user.id}`}
                        key={user.id}
                        className="panel-container"
                    >
                        <UserForm user={user}/>
                    </Paper>
                )
            );
        } else {
            return (
                <Paper className="panel-container" >
                    <UserForm/>
                </Paper>
            );
        }
    }
}

const mapStateToProps = state => ({
    users: state.userReducer.users,
    selectedUser: state.userReducer.selectedUser
});

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserPanel);
