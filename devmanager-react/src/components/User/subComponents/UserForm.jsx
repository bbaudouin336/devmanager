import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Chip from '@material-ui/core/Chip';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import ControlPointOutlinedIcon from '@material-ui/icons/ControlPointOutlined';

import validator from 'validator';

import {BUTTONS, Globals} from "../../../constantes/Globals";
import {UserGlobals} from "../../../constantes/UserGlobals";

import {createOrUpdate, updateFormUser} from '../../../actions/UserAction';
import * as tools from '../../../utils/Tools';


class UserForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {
                lastname: "",
                firstname: "",
                registrationNumber: "",
                profile: {id: "", label: ""},
                skills: []
            },
            errors: {
                errorLastname: false,
                errorFirstname: false,
                errorRegistrationNumber: false,
                errorProfile: false,
                errorSkills: false
            },
            skillToAdd: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.reinitErrorsState = this.reinitErrorsState.bind(this);
        this.addChip = this.addChip.bind(this);
        this.deleteChip = this.deleteChip.bind(this);
    }

    componentDidMount() {
        if (this.props.user !== undefined) {
            this.setState({user: Object.assign({}, this.props.user)});
        }
    }

    addChip() {
        let value = this.state.skillToAdd;
        if (value !== undefined && value !== "") {
            let skill = {id: "", label: value};
            this.setState(prevState => {
                let user = Object.assign({}, prevState.user);
                user.skills.push(skill);
                return {user};
            });
            this.setState({skillToAdd: ""});
        }
    }

    deleteChip(event, label) {

        let skills = this.state.user.skills;
        let indexSkill = tools.findIndexByLabel(this.state.user.skills, label);
        if (skills.length > 0) {
            if (indexSkill !== -1) {
                skills.splice(indexSkill, 1);
            }
        }
        this.setState(prevState => {
            let user = Object.assign({}, prevState.user);
            user.skills = skills;
            return {user};
        });
    }

    reinitErrorsState() {
        let initialErrors = {
            errorLastname: false,
            errorFirstname: false,
            errorRegistrationNumber: false,
            errorProfile: false,
            errorSkills: false,
        };
        this.setState({errors: initialErrors});

    }

    handleChange(event) {
        let target = event.target;

        if (target.id === "lastname") {
            this.setState(prevState => {
                let user = Object.assign({}, prevState.user);
                user.lastname = target.value;
                return {user};
            });
        } else if (target.id === "firstname") {
            this.setState(prevState => {
                let user = Object.assign({}, prevState.user);
                user.firstname = target.value;
                return {user};
            });
        } else if (target.id === "registrationNumber") {
            this.setState(prevState => {
                let user = Object.assign({}, prevState.user);
                user.registrationNumber = target.value;
                return {user};
            });
        } else if (target.id === "profile") {
            this.setState(prevState => {
                let user = Object.assign({}, prevState.user);

                user.profile = {id: 1, label: target.value};
                return {user};
            });
        }
        else if (target.id === "skill") {
            this.setState({skillToAdd: target.value});
        }
    }

    handleSubmit() {
        this.reinitErrorsState();
        let isEverythingOK = true;
        if (validator.isEmpty(this.state.user.lastname)) {
            this.setState({errorLastname: true});
            isEverythingOK = false;
        } else {
            if (!validator.isAlpha(this.state.user.lastname, 'fr-FR')) {
                this.setState({errorLastname: true});
                isEverythingOK = false;
            }
        }

        if (validator.isEmpty(this.state.user.firstname)) {
            this.setState({errorFirstname: true});
            isEverythingOK = false;
        } else {
            if (!validator.isAlpha(this.state.user.firstname, 'fr-FR')) {
                this.setState({errorFirstname: true});
                isEverythingOK = false;
            }
        }

        if (validator.isEmpty(this.state.user.registrationNumber)) {
            this.setState({errorRegistrationNumber: true});
            isEverythingOK = false;
        }
        if (this.state.user.profile === undefined) {
            this.setState({errorProfile: true});
            isEverythingOK = false;
        }

        if (isEverythingOK) {
            this.props.createOrUpdate(this.state.user);
        }

        return isEverythingOK;

    }

    render() {
        return (
            <form noValidate autoComplete="off" onSubmit={this.handleSubmit} action="#">
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <TextField
                            required
                            id="lastname"
                            label={UserGlobals.lastname}
                            margin="normal"
                            value={this.state.user.lastname}
                            onChange={this.handleChange}
                            error={this.state.errorLastname}
                        />
                        {(this.state.errorLastname) &&
                        <div>
                            <Typography component={"p"} color={"error"}>{Globals.errorFieldEmpty}</Typography>
                            <Typography component={"p"} color={"error"}>{Globals.errorFieldNotAlpha}</Typography>
                        </div>
                        }

                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            required
                            id="firstname"
                            label={UserGlobals.firstname}
                            margin="normal"
                            value={this.state.user.firstname}
                            onChange={this.handleChange}
                            error={this.state.errorFirstname}
                        />
                        {(this.state.errorFirstname) &&
                        <div>
                            <Typography component={"p"} color={"error"}>{Globals.errorFieldEmpty}</Typography>
                            <Typography component={"p"} color={"error"}>{Globals.errorFieldNotAlpha}</Typography>
                        </div>
                        }
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            required
                            id="registrationNumber"
                            label={UserGlobals.registrationNumber}
                            margin="normal"
                            value={this.state.user.registrationNumber}
                            onChange={this.handleChange}
                            error={this.state.errorRegistrationNumber}
                        />
                        {(this.state.errorRegistrationNumber) &&
                        <Typography component={"p"} color={"error"}>{Globals.errorFieldEmpty}</Typography>
                        }
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            required
                            id="profile"
                            label={UserGlobals.profile}
                            margin="normal"
                            value={this.state.user.profile.label}
                            onChange={this.handleChange}
                            error={this.state.errorProfile}
                        />
                        {(this.state.errorProfile) &&
                        <Typography component={"p"} color={"error"}>{Globals.errorFieldEmpty}</Typography>
                        }
                    </Grid>
                    <Grid item xs={6}>
                        <Paper className="panel-container">
                            <Box>
                            <Typography component={"h3"}>{UserGlobals.skills}</Typography>
                            {
                                this.state.user.skills.map(skill =>
                                    <Chip label={skill.label} key={skill.id} onDelete={e => this.deleteChip(e, skill.label)}/>
                                )
                            }
                            </Box>
                            <Box>
                            <InputBase
                                id="skill"
                                placeholder={UserGlobals.addSkill}
                                margin="normal"
                                value={this.state.skillToAdd}
                                onChange={this.handleChange}
                                className="inputbase"
                            />
                            {(this.state.skillToAdd !== "") &&

                            <IconButton onClick={this.addChip}>
                                <ControlPointOutlinedIcon className="btn-add"/>
                            </IconButton>
                            }
                            </Box>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                    </Grid>
                    <Grid item xs={6}>
                        {(this.state.user.id !== undefined) &&
                        <Button variant="contained" color="secondary" className="btn">
                            {BUTTONS.buttonDelete}
                            <DeleteIcon/>
                        </Button>
                        }
                    </Grid>
                    <Grid item xs={3}>
                        <Button variant="contained" size="small" className="btn btn-secondary">
                            Annuler
                        </Button>
                    </Grid>
                    <Grid item xs={3}>
                        <Button variant="contained" size="small" className="btn btn-primary" type="submit">
                            Valider
                        </Button>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

const mapStateToProps = state => ({
    userForm: state.userReducer.userForm
});

const mapDispatchToProps = dispatch => bindActionCreators({createOrUpdate, updateFormUser}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);
