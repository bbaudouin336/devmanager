import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';

import UserReducer from '../reducers/UserReducer';
import ProjectReducer from '../reducers/ProjectReducer';
import ApplicationReducer from '../reducers/ApplicationReducer';


const reducer = combineReducers({
    userReducer: UserReducer,
    projectReducer: ProjectReducer,
    applicationReducer: ApplicationReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
