/**
 * Retourne l'index de l'objet en fonction de son id
 * @param {*} array
 * @param {*} id
 */
export const findIndexById = function (array, id) {

    var i = 0;
    let index = -1;
    if (array) {
        array.forEach(o => {
            if (o.id === id) {
                index = i;
                return;
            }
            i++;
        })
    }
    return index;
};

/**
 * Retourne l'index de l'objet en fonction de son id
 * @param {*} array
 * @param {*} id
 */
export const findIndexByLabel = function (array, label) {

    let i = 0;
    let index = -1;
    if (array) {
        array.forEach(o => {
            if (o.label === label) {
                index = i;
                return;
            }
            i++;
        })
    }
    return index;
};
