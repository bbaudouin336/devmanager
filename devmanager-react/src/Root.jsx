import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MainPage from './components/MainPage';


class Root extends React.Component {
  render() {

    return (
      <Switch>
        <Route exact path='/' component={MainPage} />
      </Switch>
    );
  }
}



export default Root;


