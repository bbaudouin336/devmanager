import * as React from 'react';
import Root from './Root';

import store from './stores/GlobalStore';
import {Provider} from 'react-redux';

class App extends React.Component {

    render() {
        return (
            <Provider store={store}>
                <Root/>
            </Provider>
        );
    }
}

export default App;
